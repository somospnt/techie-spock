package com.somospnt.repository;

import com.somospnt.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long> {

    boolean existsByNombre(String nombre);
}
