package com.somospnt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieSpockApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieSpockApplication.class, args);
	}
}
