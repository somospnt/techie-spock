package com.somospnt.service.impl;

import com.somospnt.domain.Persona;
import com.somospnt.repository.PersonaRepository;
import com.somospnt.service.PersonaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PersonaServiceImpl implements PersonaService {

    private final PersonaRepository personaRepository;

    public PersonaServiceImpl(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    @Override
    public List<Persona> buscarTodas() {
        return personaRepository.findAll();
    }

    @Override
    public Persona buscarPorId(Long id) {
        Persona persona = personaRepository.findOne(id);
        if (persona == null) {
            String mensajeError = new StringBuilder("No existe la persona con el id ").append(id).toString();
            throw new NoSuchElementException(mensajeError);
        } else {
            return persona;
        }
    }

    @Override
    public boolean existeConNombre(String nombre) {
        return personaRepository.existsByNombre(nombre);
    }

    @Override
    public Persona guardar(Persona persona) {
        return personaRepository.save(persona);
    }
}
