package com.somospnt.service;

import com.somospnt.domain.Persona;

import java.util.List;

public interface PersonaService {

    List<Persona> buscarTodas();

    Persona buscarPorId(Long id);

    boolean existeConNombre(String nombre);

    Persona guardar(Persona persona);
}
