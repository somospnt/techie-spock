package com.somospnt.service

import com.somospnt.domain.Persona
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

@SpringBootTest
@Transactional
class PersonaServiceTest extends Specification {

    @Autowired
    PersonaService personaService

    def setup() {
        println "Comenzando ejecucion de feature"
    }

    def cleanup() {
        println "Terminando ejecucion de feature"
    }

    def "buscar todas, con personas existentes, devuelve todas las personas"() {
        when:
        List<Persona> personas = personaService.buscarTodas()

        then:
        3 == personas.size()
        1 == personas[0].id
    }

    def "buscar por id, con id 1, devuelve persona con id 1"() {
        given:
        Long id = 1L

        when:
        Persona persona = personaService.buscarPorId(id)

        then:
        id == persona.id
    }

    def "buscar por id, con id 6, lanza NotSuchElementException"() {
        given:
        Long id = 6L

        when:
        personaService.buscarPorId(id)

        then:
        thrown(NoSuchElementException)
    }

    def "existe con nombre, con nombre p1, devuelve true"() {
        given:
        String nombreBuscado = "p1"

        when:
        boolean existeConNombreP1 = personaService.existeConNombre(nombreBuscado)

        then:
        existeConNombreP1
    }

    def "existe con nombre, con nombre inexistente, devuelve false"() {
        given:
        String nombreBuscado = "inexistente"

        when:
        boolean existeConNombreInexistente = personaService.existeConNombre(nombreBuscado)

        then:
        !existeConNombreInexistente
    }

    //Crear feature para testear el guardado de una persona


    private crearPersona(def nombre, def edad) {
        def persona = new Persona()
        persona.nombre = nombre
        persona.edad = edad

        return persona
    }
}