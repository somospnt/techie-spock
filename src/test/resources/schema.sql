drop table if exists persona;

create table persona(
  id bigint primary key identity,
  nombre varchar(50) not null,
  edad integer not null
);

insert into persona values
(1, 'p1', 20),
(2, 'p2', 21),
(3, 'p3', 22);